# Welcome to Coursework Group 9 | Team Research & Development

# Title : Data Visualization and Analysis for Road Accidents in UK

# Group Number : 9

# Group Members
-    Name : Aron Samuel Georgekutty 
-    Name : Mohammed Ashiq Rajja Mohammed Yousuf  
-    Name : Jayashree Ravi 
-    Name : Sanjay Jose Kandathil 
-    Name : Geordin Jose 

# Data Set URL
-   https://www.kaggle.com/daveianhickey/2000-16-traffic-flow-england-scotland-wales#accidents_2012_to_2014.csv

# Purpose

- This Repository is for analysing and facilitating the course work 7COM1079-0901-2019 Of Team Reserach Development project held at University of Hertforshire,United Kingdom.This Repository provides details of data visualization using R with data set for Road Accidents and contains the statistical report generated for the road accidents happening in UK. 
